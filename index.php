<!DOCTYPE html>
<html lang="it">
    <?php
        session_start();
        if(isset($_POST["usr"]) && isset($_POST["pwd"]))
        {
            require($_SERVER['DOCUMENT_ROOT'] ."/php/settings.php");
            $passwordCrypt = md5(md5($_POST["pwd"]));
            $sql = "SELECT * FROM Users WHERE BINARY user = '". $_POST["usr"] ."' && BINARY password = '" . $passwordCrypt ."';";
            $result = $connection->query($sql);
            $list = array();
            $no=0;
            while($row = $result->fetch(PDO::FETCH_ASSOC))
            {
                $list[] = $row;
            }
            if(count($list)!=1)
            {
                $no=1;
            }
            else
            {
                $_SESSION["login"]="OK";
                header("location: /home.php");
            }
        }
    ?>
    <head>
        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="css/style.css"/>
        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>AirHome</title>
        <link rel="shortcut icon" type="image/png" href="/img/favicon.png"/>
    </head>
    <body onload="animazione()">
        <center><span id="logo"/></center>
        <div style="margin-top: 0px; margin-bottom:40px;">
            <center>
            <h1 style="margin: 0px;">Login</h1>
            <?php
                if(isset($no) && $no==1)
                    echo "<h3 class=\"errore\">Dati errati</h3>";
            ?>
            <form class="col s12" method="post" style="width:40%;padding-left:40px;" action="index.php">
                <div class="input-field col s12">
                    <input id="user" type="text" style="color:white;font-size:20px;" name="usr" autocomplete="off">
                    <label for="user" style="color:white;font-size:20px;">Username</label>
                </div>
                <div class="input-field col s12">
                    <input id="password" type="password" style="color:white;font-size:20px;" name="pwd" autocomplete="off">
                    <label for="password" style="color:white;font-size:20px;">Password</label>
                </div>
                <input class="waves-effect waves-light btn" type="submit" value="Login">
            </form>
            </center>
        </div>
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="js/materialize.min.js"></script>
        <script src ="js/AirHome/p5.js"></script>
        <script src ="js/AirHome/sketch.js"></script>
        <script src ="js/AirHome/vehicle.js"></script>
        <script>
            function animazione() {
                var dispositivo = navigator.userAgent;
                if(dispositivo.includes("Windows Phone"))
                {
                    document.getElementById("logo").style.visibility="hidden";
                    var element = document.getElementById("logo");
                    element.outerHTML = "";
                    delete element;
                }
            }
            
        </script>
    </body>
</html>