<!DOCTYPE html>
<html lang="it">
    <?php
        session_start();
        if(!isset($_SESSION["login"]) || $_SESSION["login"] != "OK")
        {
            header("location: /index.php");
        }
        else 
        {
            require($_SERVER['DOCUMENT_ROOT'] ."/php/weather.php");
            $meteo = new MeteoEsterno();
        }    
    ?>
    <head>
        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection" />
        <link type="text/css" rel="stylesheet" href="css/style.css" />
        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>AirHome Dashboard</title>         
        <link rel="shortcut icon" type="image/png" href="/img/favicon.png"/>
    </head>
    <body>
        <div id="nav"></div>
        <h3>Meteo esterno</h3>
        <div class="row">
            <div class="col s4">
                <center>
                    <img <?php echo "src=\"img/weather/weather/" . $meteo->getStatusImage() . ".png\""; ?> alt="Meteo" height="150" width="150" style="float:right;">
                </center>
            </div>
            <div class="col s8 meteo">
                <p>
                    <font size ="30px">
                    <?php
                        require($_SERVER['DOCUMENT_ROOT'] ."/php/settings.php");
                        $result = $connection->query("SELECT Valore FROM Impostazioni WHERE Impostazione = 'Comune';")->fetch();
                        $nome = $result["Valore"];
                        echo $nome;
                    ?>
                    </font>
                    <br>
                    &emsp;<?php echo $meteo->descrizione();?><br>
                    &emsp;Temperatura: <?php echo $meteo->temp();?><br>
                    &emsp;Umidit&agrave;: <?php echo $meteo->humidity();?><br>
                    &emsp;Precipitazioni: <?php echo $meteo->pioggia();?><br>
                    &emsp;Vento: <?php echo $meteo->vento();?><br>
                    &emsp;Last update: <?php echo $meteo->lastUpdate();?>
                </p>
            </div>
        </div>
        <br>
        <div style="float:left;margin-left: 20px;" class="meteo">
            <p>
                Temperatura massima: <?php echo $meteo->tempMax();?><br>
                Temperatura minima: <?php echo $meteo->tempMin();?><br>
                Coperto: <?php echo $meteo->nuvole();?><br>
                Pressione: <?php echo $meteo->pressure();?>
            </p>
        </div>
        <div style="float:left;margin-left: 20px;" class="meteo">
            <p>
                Alba: <?php echo $meteo->alba();?><br>
                Tramonto: <?php echo $meteo->tramonto();?><br>
            </p>
        </div>
        <div style="float:left;margin-left: 20px;" class="meteo">
            <p>
                <img <?php echo "src=\"img/weather/moon/" . $meteo->luna(1) . ".png\""; ?> alt="Luna" width="40px" heigth="40px">
                <?php echo $meteo->luna();?>
            </p>
        </div>
        <div style="float:left;margin-left: 20px;" class="meteo">
            <p>
                Vento: <?php echo $meteo->vento();?><br>
                Direzione vento: <?php echo $meteo->vento(2);?> <img <?php echo "src=\"img/weather/wind/" . $meteo->vento(1) . ".png\""; ?> alt="Vento" width="40px" heigth="40px">
            </p>
        </div>
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="js/materialize.min.js"></script>
        <script>
            $(function () {
                $("#nav").load("nav.html");
            });
        </script>
    </body>
</html>