<!DOCTYPE html>
<html lang="it">
    <?php
        session_start();
        if(!isset($_SESSION["login"]) || $_SESSION["login"] != "OK")
        {
            header("location: /index.php");
        }     
    ?>
    <head>
        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="../css/materialize.min.css"  media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="../css/style.css"/>
        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>AirHome Dashboard</title>         
        <link rel="shortcut icon" type="image/png" href="/img/favicon.png"/>
    </head>
    <body>
        <!-- Navbar goes here -->
        <div id="nav"></div>
        <!-- Page Layout here -->
        <div class="row" style="margin-top:1%;">
            <h3>Impostazioni</h3>
            <br>
            <div class="col s12 m4 l3" style="background-color: #1F9C8B;min-height: 100%;"> <!-- Note that "m4 l3" was added -->
                    <div class="collection">
                        <a href="../impostazioni/notifiche.php" class="collection-item">Notifiche</a>
                        <a href="../impostazioni/user.php" class="collection-item">Account</a>
                        <a href="../impostazioni/stats.php" class="collection-item active">Statistiche</a>
                    </div>
            </div>
            <div class="col s12 m8 l9">
                <div>
                    <center>
                        <h5 id="stats">Giorno inizio misurazioni</h5>
                        <br>
                        <p>
                            <?php
                                 require($_SERVER['DOCUMENT_ROOT'] ."/php/settings.php");
                                 $result = $connection->query("SELECT DATE_FORMAT(Ora, '%H:%i:%s alle %d/%m/%Y') AS 'Ora' FROM Misurazioni WHERE ID = (SELECT min(ID) FROM Misurazioni);")->fetch();
                                 echo "AirHome ha iniziato a misurare il " . $result["Ora"] ."<br>";
                                 $result = $connection->query("SELECT COUNT(*) AS 'numero' FROM Misurazioni;")->fetch();
                                 echo "Attualmente ci sono " . $result["numero"] ." rilevazioni";
                            ?>
                        </p>
                    </center>
                </div>
                <br>
                <div>
                    <center>
                        <h5 id="stats">Min/Max Rilevazioni</h5>
                        <table class="responsive-table" style="width:85%">
                            <thead>
                                <tr>
                                    <th>Misurazione</th>
                                    <th>Minima</th>
                                    <th>Data</th>
                                    <th>Massima</th>
                                    <th>Data</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    require($_SERVER['DOCUMENT_ROOT'] ."/php/settings.php");
                                    $result = $connection->query("SELECT Temperatura AS TemperaturaMAX,DATE_FORMAT(Ora, '%H:%i:%s %d/%m/%Y') AS Ora FROM Misurazioni WHERE temperatura = (SELECT MAX(Temperatura) FROM Misurazioni) ORDER BY Ora LIMIT 1;")->fetch();
                                    $TMax=array($result["TemperaturaMAX"]."°C",$result["Ora"]);
                                    $result = $connection->query("SELECT Temperatura AS TemperaturaMIN,DATE_FORMAT(Ora, '%H:%i:%s %d/%m/%Y') AS Ora FROM Misurazioni WHERE temperatura = (SELECT MIN(Temperatura) FROM Misurazioni) ORDER BY Ora LIMIT 1;")->fetch();
                                    $TMin=array($result["TemperaturaMIN"]."°C",$result["Ora"]);
                                    
                                    $result = $connection->query("SELECT Umidita AS UmiditaMAX,DATE_FORMAT(Ora, '%H:%i:%s %d/%m/%Y') AS Ora FROM Misurazioni WHERE Umidita = (SELECT MAX(Umidita) FROM Misurazioni) ORDER BY Ora LIMIT 1;")->fetch();
                                    $UMax=array($result["UmiditaMAX"]."%",$result["Ora"]);
                                    $result = $connection->query("SELECT Umidita AS UmiditaMIN,DATE_FORMAT(Ora, '%H:%i:%s %d/%m/%Y') AS Ora FROM Misurazioni WHERE Umidita = (SELECT MIN(Umidita) FROM Misurazioni) ORDER BY Ora LIMIT 1;")->fetch();
                                    $UMin=array($result["UmiditaMIN"]."%",$result["Ora"]);
                                    
                                    $result = $connection->query("SELECT Pressione AS PressioneMAX,DATE_FORMAT(Ora, '%H:%i:%s %d/%m/%Y') AS Ora FROM Misurazioni WHERE Pressione = (SELECT MAX(Pressione) FROM Misurazioni) ORDER BY Ora LIMIT 1;")->fetch();
                                    $PMax=array($result["PressioneMAX"]." Pa",$result["Ora"]);
                                    $result = $connection->query("SELECT Pressione AS PressioneMin,DATE_FORMAT(Ora, '%H:%i:%s %d/%m/%Y') AS Ora FROM Misurazioni WHERE Pressione = (SELECT MIN(Pressione) FROM Misurazioni) ORDER BY Ora LIMIT 1;")->fetch();
                                    $PMin=array($result["PressioneMin"]." Pa",$result["Ora"]);

                                    $result = $connection->query("SELECT CO2 AS CO2MAX,DATE_FORMAT(Ora, '%H:%i:%s %d/%m/%Y') AS Ora FROM Misurazioni WHERE CO2 = (SELECT MAX(CO2) FROM Misurazioni) ORDER BY Ora LIMIT 1;")->fetch();
                                    $CMax=array($result["CO2MAX"]." ppm",$result["Ora"]);
                                    $result = $connection->query("SELECT CO2 AS CO2MIN,DATE_FORMAT(Ora, '%H:%i:%s %d/%m/%Y') AS Ora FROM Misurazioni WHERE CO2 = (SELECT MIN(CO2) FROM Misurazioni) ORDER BY Ora LIMIT 1;")->fetch();
                                    $CMin=array($result["CO2MIN"]." ppm",$result["Ora"]);
                                    
                                    echo "<tr><td>Temperatura</td><td>$TMin[0]</td><td>$TMin[1]</td><td>$TMax[0]</td><td>$TMax[1]</td></tr>";
                                    echo "<tr><td>Umidit&agrave;</td><td>$UMin[0]</td><td>$UMin[1]</td><td>$UMax[0]</td><td>$UMax[1]</td></tr>";
                                    echo "<tr><td>Pressione</td><td>$PMin[0]</td><td>$PMin[1]</td><td>$PMax[0]</td><td>$PMax[1]</td></tr>";
                                    echo "<tr><td>CO2</td><td>$CMin[0]</td><td>$CMin[1]</td><td>$CMax[0]</td><td>$CMax[1]</td></tr>";
                                ?>
                            </tbody>
                        </table>
                    </center>
                </div>
                <br>
                <div>
                    <center>
                        <h5 id="device">Device collegati al server notifiche</h5>
                        <?php
                            $sql = "SELECT * FROM NotificheUWP";
                            $result = $connection->query($sql);
                            echo "<table class=\"responsive-table\" style=\"width:85%;\">
                                            <thead>
                                                <tr>
                                                    <th>IDapp</th>
                                                    <th>Registrazione</th>
                                                    <th>Aggiornamento</th>
                                                    <th>Silenzioso</th>
                                                </tr>
                                            </thead><tbody>";
                            while($row = $result->fetch(PDO::FETCH_OBJ))
                            {
                                echo"<tr>
                                        <td>$row->IDapp</td>
                                        <td>$row->Registrazione</td>
                                        <td>$row->Aggiornamento</td>
                                        <td>$row->silenzioso</td>
                                    </tr>";
                            }
                            echo"</tbody></table>";
                        ?>
                    </center>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="../js/materialize.min.js"></script>
        <script>
            $(function(){
                $("#nav").load("../nav.html");
            });
        </script>
    </body>
</html>