<!DOCTYPE html>
<html lang="it">
    <?php
        session_start();
        if(!isset($_SESSION["login"]) || $_SESSION["login"] != "OK")
        {
            header("location: /index.php");
        }     
    ?>
    <head>
        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="../css/materialize.min.css"  media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="../css/style.css"/>
        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>AirHome Dashboard</title>         
        <link rel="shortcut icon" type="image/png" href="/img/favicon.png"/>
    </head>
    <body>
        <!-- Navbar goes here -->
        <div id="nav"></div>
        <!-- Page Layout here -->
        <div class="row" style="margin-top:1%;">
            <h3>Impostazioni</h3>
            <br>
            <div class="col s12 m4 l3" style="background-color: #1F9C8B; min-height: 100%;"> <!-- Note that "m4 l3" was added -->
                <div class="collection">
                    <a href="../impostazioni/notifiche.php" class="collection-item">Notifiche</a>
                    <a href="../impostazioni/user.php" class="collection-item active">Account</a>
                    <a href="../impostazioni/stats.php" class="collection-item">Statistiche</a>
                </div>
            </div>
            <div class="col s12 m8 l9">
                <?php require($_SERVER['DOCUMENT_ROOT'] ."/php/settings.php"); ?>
                <center>
                    <div>
                        <form class="col s12" method="post" style="text-align:center;" action="/php/changecountry.php" onsubmit="if (this.newcountry.value=='') {alert('Aggiungi un paese per procedere!'); return false;}">
                            <div style="width:300px;display:inline-block;">
                                <h5 id="paese">Imposta il paese</h5>
                                <div class="input-field col s12">
                                    <input id="newcountry" type="text" style="color:white;font-size:20px;" name="newcountry" autocomplete="off">
                                    <label for="newcountry" style="color:white;font-size:20px;">Inserisci il paese</label>
                                    <input class="waves-effect waves-light btn" type="submit" value = "Cambia Paese">
                                </div>
                            </div>
                        </form>
                    </div>
                    <br>
                    <div>
                        <form class="col s12" method="post" style="text-align:center;" action="/php/changepwd.php">
                            <div style="width:300px;display:inline-block;">
                                <h5 id="cambiapwd">Cambia password</h5>    
                                <div class="input-field col s12">
                                    <input id="oldpassword" type="password" style="color:white;font-size:20px;" name="oldpwd">
                                    <label for="oldpassword" style="color:white;font-size:20px;">Vecchia Password</label>
                                </div>
                                <div class="input-field col s12">
                                    <br>
                                    <input id="newpassword" type="password" style="color:white;font-size:20px;" name="newpwd">
                                    <label for="newpassword" style="color:white;font-size:20px;">Nuova Password</label>
                                    <input class="waves-effect waves-light btn" type="submit" value = "Cambia password">
                                </div>
                            </div>
                        </form>
                    </div>
                </center>
            </div>
        </div>
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="../js/materialize.min.js"></script>
        <script>
            $(function(){
                $("#nav").load("../nav.html");
            });
        </script>
    </body>
</html>