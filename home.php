<!DOCTYPE html>
<html lang="it">
    <?php
        session_start();
        if(!isset($_SESSION["login"]) || $_SESSION["login"] != "OK")
        {
            header("location: /index.php");
        }     
    ?>
    <head>
        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection" />
        <link type="text/css" rel="stylesheet" href="css/style.css" />
        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>AirHome Dashboard</title>         
        <link rel="shortcut icon" type="image/png" href="/img/favicon.png"/>
    </head>
    <body>
        <div id="nav"></div>
        <h3>Dashboard</h3>
        <div class="row">
            <div class="col s12 m12">
                <div class="card teal darken-4" style="width: 100%;<!--68-->">
                    <div class="card-content white-text">
                        <span class="card-title">Ultimi dati</span>
                        <?php
                            require($_SERVER['DOCUMENT_ROOT'] ."/php/settings.php");
                            $result = $connection->query("SELECT Temperatura,Umidita,Pressione,CO2,DATE_FORMAT(Ora, '%H:%i:%s %d/%m/%Y') AS Ora FROM Misurazioni WHERE ID = (SELECT MAX(ID) FROM Misurazioni);")->fetch(); 
                            echo"<center><p class=\"white-text\">
                            Temperatura: ". $result["Temperatura"] . "°C<br>Umidità: " . $result["Umidita"] ."%<br> 
                            Pressione: " . $result["Pressione"] . " Pa<br> 
                            CO2: " . $result["CO2"] ." ppm<br>Ultimo aggiornamento: " . $result["Ora"] . "</p>";
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col m5">
                <div class="card teal darken-4">
                    <div class="card-content white-text">
                        <span class="card-title">Qualit&agrave; dell'aria</span>
                        <center>
                            <?php
                                if($result["CO2"]>=1500)
                                {
                                    echo "<div id =\"co2rosso\"></div>";
                                }
                                else if($result["CO2"]>=1600)
                                {
                                    echo "<div id =\"co2arancione\"></div>";
                                }
                                else if($result["CO2"]>=1100)
                                {
                                    echo "<div id =\"co2giallo\"></div>";
                                }
                                else if($result["CO2"]>=700)
                                {
                                    echo "<div id =\"co2verdechiaro\"></div>";
                                }
                                else 
                                {
                                    echo "<div id =\"co2verde\"></div>";
                                }
                                echo "<p>Livello CO2: " . $result["CO2"] ." ppm</p>";
                            ?>
                        </center>
                    </div>
                </div>
            </div>
            <div class="col m5">
                <div class="card teal darken-4">
                    <div class="card-content white-text">
                        <span class="card-title">Indice Humidex</span>
                        <center>
                            <?php
                                function Humidex($temp, $umid)
                                {
                                    $kelvin = $temp + 273;
                                    $eTs = pow(10, ((-2937.4 / $kelvin) - 4.9283 * log($kelvin) / log(10) + 23.5471));//log = ln
                                    $eTd = $eTs * $umid / 100;
                                    $hx = round($temp + (($eTd - 10) * 5 / 9));
                                    return intval($hx);
                                }
                                $index = Humidex($result["Temperatura"],$result["Umidita"]);
                                echo "<a style=\"display:block\" href=\"humidex.php\">";
                                if($index<=29)
                                {
                                    echo "<div id =\"co2verde\"></div>";
                                }
                                else if($index>29 && $index<=34)
                                {
                                    echo "<div id =\"humblu\"></div>";
                                }
                                else if($index>34 && $index <=39)
                                {
                                    echo "<div id =\"humviola\"></div>";
                                }
                                else if($index>39 && $index <=45)
                                {
                                    echo "<div id =\"co2giallo\"></div>";
                                }
                                else if($index>45 && $index <=53)
                                {
                                    echo "<div id =\"co2arancione\"></div>";
                                }
                                else 
                                {
                                    echo "<div id =\"co2rosso\"></div>";
                                }

                                echo "</a><p>Livello:  " . $index ."</p>";
                            ?>
                        </center>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="js/materialize.min.js"></script>
        <script>
            $(function () {
                $("#nav").load("nav.html");
            });
        </script>
    </body>
</html>