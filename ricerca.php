<html>
<?php
    session_start();
    if(!isset($_SESSION["login"]) || $_SESSION["login"] != "OK")
    {
        header("location: /index.php");
    }
?>

    <head>
        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="css/style.css"/>
        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>AirHome Dashboard</title>         
        <link rel="shortcut icon" type="image/png" href="/img/favicon.png"/>
    </head>

    <body>
        <div id="nav"></div>
        <div style="margin-top: 40px; margin-bottom:40px;">
            <center>
                <?php
                    if(isset($_GET['giorno']))
                    {
                        require($_SERVER['DOCUMENT_ROOT'] ."/php/settings.php");
                        $giorno = urldecode($_GET['giorno']);
                        $sql = "SELECT  COUNT(*) AS 'N',
                                        TRUNCATE(MIN(Temperatura),2) AS 'TempMIN',
                                        TRUNCATE(MAX(Temperatura),2) AS 'TempMAX',
                                        TRUNCATE(MIN(Umidita),2) AS 'UmidMIN',
                                        TRUNCATE(MAX(Umidita),2) AS 'UmidMAX',
                                        TRUNCATE(MIN(Pressione),2) AS 'PressMIN',
                                        TRUNCATE(MAX(Pressione),2) AS 'PressMAX',
                                        TRUNCATE(MIN(CO2),2) AS 'CO2MIN',
                                        TRUNCATE(MAX(CO2),2) AS 'CO2MAX',
                                        TRUNCATE(AVG(Temperatura),2) AS 'TempMED',
                                        TRUNCATE(AVG(Umidita),2) AS 'UmidMED',
                                        TRUNCATE(AVG(Pressione),2) AS 'PressMED',
                                        TRUNCATE(AVG(CO2),2) AS 'CO2MED'
                                        FROM
                                            Misurazioni
                                        WHERE
                                            DATE(ora) = '$giorno';";
                        $result = $connection->query($sql)->fetch();

                        $tempMax = $result['TempMAX'];
                        $tempMin = $result['TempMIN'];
                        $tempMed = $result['TempMED'];
                        $umidMax = $result['UmidMAX'];
                        $umidMin = $result['UmidMIN'];
                        $umidMed = $result['UmidMED'];
                        $presMax = $result['PressMAX'];
                        $presMin = $result['PressMIN'];
                        $presMed = $result['PressMED'];
                        $co2Max = $result['CO2MAX'];
                        $co2Min = $result['CO2MIN'];
                        $co2Med = $result['CO2MED'];
                        $numero = $result['N'];

                        if($numero!=0)
                        {
                            $giornovett = explode("/", $giorno);
                            echo "<p>Misurazioni giorno: $giornovett[2]/$giornovett[1]/$giornovett[0]</p>";
                            echo "<p>Numero rilevazioni in questo giorno: $numero</p>";

                            echo "<table class=\"responsive-table\" style=\"width:70%;\">
                                    <thead>
                                        <tr>
                                            <th>Misurazione</th>
                                            <th>Minima</th>
                                            <th>Massima</th>
                                            <th>Media</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td><a href=\"ricerca.php?giorno=$giorno#temperatura\">Temperatura</a></td>
                                        <td>$tempMin</td>
                                        <td>$tempMax</td>
                                        <td>$tempMed</td>
                                    </tr>
                                    <tr>
                                        <td><a href=\"ricerca.php?giorno=$giorno#umidita\">Umidit&agrave;</a></td>
                                        <td>$umidMin</td>
                                        <td>$umidMax</td>
                                        <td>$umidMed</td>
                                    </tr>
                                    <tr>
                                        <td><a href=\"ricerca.php?giorno=$giorno#pressione\">Pressione</a></td>
                                        <td>$presMin</td>
                                        <td>$presMax</td>
                                        <td>$presMed</td>
                                    </tr>
                                    <tr>
                                        <td><a href=\"ricerca.php?giorno=$giorno#co2\">CO2</a></td>
                                        <td>$co2Min</td>
                                        <td>$co2Max</td>
                                        <td>$co2Med</td>
                                    </tr>
                                    </tbody>
                                </table>";

                            echo "<center><a class=\"waves-effect waves-light btn\" href=\"/php/exportcsv.php?giorno=" . $giorno . "\"><i class=\"material-icons left\">file_download</i>Scarica report CSV</a></center>";
                            echo "<h4 style=\"margin-bottom: 0px;\" id=\"temperatura\">Temperatura</h4>";
                            echo "<img width=80% src=\"php/charts/temp.php?data=" . $giorno . "\"/>";
                            echo "<h4 style=\"margin-bottom: 0px;\" id=\"umidita\">Umidit&agrave;</h4>";
                            echo "<img width=80% src=\"php/charts/umid.php?data=" . $giorno . "\"/>";
                            echo "<h4 style=\"margin-bottom: 0px;\" id=\"pressione\">Pressione</h4>";
                            echo "<img width=80% src=\"php/charts/press.php?data=" . $giorno . "\"/>";
                            echo "<h4 style=\"margin-bottom: 0px;\" id=\"co2\">CO2</h4>";
                            echo "<img width=80% src=\"php/charts/co2.php?data=" . $giorno . "\"/>";
                        }
                        else
                        {
                            echo "<p>Nessuna misurazione per questo giorno</p>";
                            echo "<a class=\"waves-effect waves-light btn\" href=\"./ricerca.php\">Torna indietro</a>";
                        }
                    }
                    else
                    {
                ?>
                        <p>Ricerca</p>
                        <form method="GET" action="#" style="width:35%; text-align:center" onsubmit="if (this.giorno.value=='') {alert('Seleziona il giorno prima di procedere!'); return false;}">
                            <h5>Seleziona il giorno</h5>
                            <input type="date" style="color:white;" class="datepicker" name="giorno">
                            <br>
                            <input class="waves-effect waves-light btn" type="submit" value="Conferma"></input>
                        </form>
                
                <?php
                    }
                ?>
            </center>
        </div>
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="js/materialize.min.js"></script>
        <script>
            $(document).ready(function () {
                $("#nav").load("nav.html");
                $('.datepicker').pickadate({
                    selectMonths: true, // Creates a dropdown to control month
                    selectYears: 15, // Creates a dropdown of 15 years to control year
                    format: 'yyyy/mm/dd',
                    // The title label to use for the month nav buttons
                    labelMonthNext: 'Prossimo mese',
                    labelMonthPrev: 'Mese precedente',

                    // The title label to use for the dropdown selectors
                    labelMonthSelect: 'Seleziona un mese',
                    labelYearSelect: 'Seleziona un anno',

                    // Months and weekdays
                    monthsFull: ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre'],
                    monthsShort: ['Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov', 'Dic'],
                    weekdaysFull: ['Domenica', 'Lunedì', 'Martedì', 'Mercoledì', 'Giovedì', 'Venerdì', 'Sabato'],
                    weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mer', 'Gio', 'Ven', 'Sab'],

                    // Materialize modified
                    weekdaysLetter: ['D', 'L', 'M', 'M', 'G', 'V', 'S'],

                    // Today and clear
                    today: 'Oggi',
                    clear: 'Pulisci',
                    firstDay: 'Lunedì'
                });
            })
        </script>
    </body>

</html>