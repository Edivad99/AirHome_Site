<!DOCTYPE html>
<html lang="it">
    <?php
        session_start();
        if(!isset($_SESSION["login"]) || $_SESSION["login"] != "OK")
        {
            header("location: /index.php");
        }     
    ?>
    <head>
        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection" />
        <link type="text/css" rel="stylesheet" href="css/style.css" />
        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>AirHome Dashboard</title>         
        <link rel="shortcut icon" type="image/png" href="/img/favicon.png"/>
    </head>
    <body>
        <div id="nav"></div>
        <h3>Monitoraggio</h3>
        <?php
            $date = date("Y/m/d");
            echo "&emsp;<a href=\"ricerca.php?giorno=$date\">Visualizza l'andamento di oggi</a>";
        ?>
        
        <center>
            <h4>Temperatura</h4>
            <img width=90% src="php/charts/temp.php"/>
            <h4>Umidit&agrave;</h4>
            <img width=90% src="php/charts/umid.php"/>
            <h4>Pressione</h4>
            <img width=90% src="php/charts/press.php"/>
            <h4>CO2</h4>
            <img width=90% src="php/charts/co2.php"/>
        </center>
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="js/materialize.min.js"></script>
        <script>
            $(function () {
                $("#nav").load("nav.html");
            });
        </script>
    </body>
</html>