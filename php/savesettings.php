<?php
    //var_dump($_POST);
    //echo"<br>";
    require($_SERVER['DOCUMENT_ROOT'] ."/php/settings.php");
    require($_SERVER['DOCUMENT_ROOT'] ."/php/insert/check.php");
    $notifiche = new check("0","0","0","0");

    if(isset($_POST["range"]))
    {
        $aggiornameto=0;
        //Temp
        if(!isset($_POST["Tmax"]))
        {
            //Impostazione manuale
            if(strlen($_POST["TmaxL"])>0)
            {
                $valore_nuovo = explode(" ",$_POST["TmaxL"])[0];
                $data = date("Y-m-d");
                $sql = "UPDATE personalRange SET Valore = '$valore_nuovo' WHERE Impostazione = 'Temperatura_max';
                        UPDATE personalRange SET Valore_AUTO = '' WHERE Impostazione = 'Temperatura_max';
                        UPDATE personalRange SET Modifica = '$data' WHERE Impostazione = 'Temperatura_max';";
                $connection->exec($sql);
            } 
        }
        else 
        {
            //Impostazione automatica
            $sql = "UPDATE personalRange SET Valore = 'Auto' WHERE Impostazione = 'Temperatura_max';";
            $connection->exec($sql);
            $aggiornameto=1;
        }

        if(!isset($_POST["Tmin"]))
        {
            //Impostazione manuale
            if(strlen($_POST["TminL"])>0)
            {
                $valore_nuovo = explode(" ",$_POST["TminL"])[0];
                $data = date("Y-m-d");
                $sql = "UPDATE personalRange SET Valore = '$valore_nuovo' WHERE Impostazione = 'Temperatura_min';
                        UPDATE personalRange SET Valore_AUTO = '' WHERE Impostazione = 'Temperatura_min';
                        UPDATE personalRange SET Modifica = '$data' WHERE Impostazione = 'Temperatura_min';";
                $connection->exec($sql);
            } 
        }
        else 
        {
            //Impostazione automatica
            $sql = "UPDATE personalRange SET Valore = 'Auto' WHERE Impostazione = 'Temperatura_min';";
            $connection->exec($sql);
            $aggiornameto=1;
        }

        //Umid
        if(!isset($_POST["Umax"]))
        {
            //Impostazione manuale
            if(strlen($_POST["UmaxL"])>0)
            {
                $valore_nuovo = explode(" ",$_POST["UmaxL"])[0];
                $data = date("Y-m-d");
                $sql = "UPDATE personalRange SET Valore = '$valore_nuovo' WHERE Impostazione = 'Umidita_max';
                        UPDATE personalRange SET Valore_AUTO = '' WHERE Impostazione = 'Umidita_max';
                        UPDATE personalRange SET Modifica = '$data' WHERE Impostazione = 'Umidita_max';";
                $connection->exec($sql);
            } 
        }
        else 
        {
            //Impostazione automatica
            $sql = "UPDATE personalRange SET Valore = 'Auto' WHERE Impostazione = 'Umidita_max';";
            $connection->exec($sql);
            $aggiornameto=1;
        }

        if(!isset($_POST["Umin"]))
        {
            //Impostazione manuale
            if(strlen($_POST["UminL"])>0)
            {
                $valore_nuovo = explode(" ",$_POST["UminL"])[0];
                $data = date("Y-m-d");
                $sql = "UPDATE personalRange SET Valore = '$valore_nuovo' WHERE Impostazione = 'Umidita_min';
                        UPDATE personalRange SET Valore_AUTO = '' WHERE Impostazione = 'Umidita_min';
                        UPDATE personalRange SET Modifica = '$data' WHERE Impostazione = 'Umidita_min';";
                $connection->exec($sql);
            } 
        }
        else 
        {
            //Impostazione automatica
            $sql = "UPDATE personalRange SET Valore = 'Auto' WHERE Impostazione = 'Umidita_min';";
            $connection->exec($sql);
            $aggiornameto=1;
        }

        //Pressione
        if(!isset($_POST["Pmax"]))
        {
            //Impostazione manuale
            if(strlen($_POST["PmaxL"])>0)
            {
                $valore_nuovo = explode(" ",$_POST["PmaxL"])[0];
                $data = date("Y-m-d");
                $sql = "UPDATE personalRange SET Valore = '$valore_nuovo' WHERE Impostazione = 'Press_max';
                        UPDATE personalRange SET Valore_AUTO = '' WHERE Impostazione = 'Press_max';
                        UPDATE personalRange SET Modifica = '$data' WHERE Impostazione = 'Press_max';";
                $connection->exec($sql);
            } 
        }
        else 
        {
            //Impostazione automatica
            $sql = "UPDATE personalRange SET Valore = 'Auto' WHERE Impostazione = 'Press_max';";
            $connection->exec($sql);
            $aggiornameto=1;
        }

        if(!isset($_POST["Pmin"]))
        {
            //Impostazione manuale
            if(strlen($_POST["PminL"])>0)
            {
                $valore_nuovo = explode(" ",$_POST["PminL"])[0];
                $data = date("Y-m-d");
                $sql = "UPDATE personalRange SET Valore = '$valore_nuovo' WHERE Impostazione = 'Press_min';
                        UPDATE personalRange SET Valore_AUTO = '' WHERE Impostazione = 'Press_min';
                        UPDATE personalRange SET Modifica = '$data' WHERE Impostazione = 'Press_min';";
                $connection->exec($sql);
            } 
        }
        else 
        {
            //Impostazione automatica
            $sql = "UPDATE personalRange SET Valore = 'Auto' WHERE Impostazione = 'Press_min';";
            $connection->exec($sql);
            $aggiornameto=1;
        }

        if($aggiornameto == 1)
        {
            $notifiche->updateSettingsAuto(1);
        }
        header("location: /impostazioni/notifiche.php");
    }
    if(isset($_POST["Notifiche"]))
    {
        if(isset($_POST["Temp"]))
        {
            $sql = "UPDATE Impostazioni SET Valore = 'SI' WHERE Impostazione = 'Temp_Notify';";
            $connection->exec($sql);
        }
        else 
        {
            $sql = "UPDATE Impostazioni SET Valore = 'NO' WHERE Impostazione = 'Temp_Notify';";
            $connection->exec($sql);
        }

        if(isset($_POST["Umid"]))
        {
            $sql = "UPDATE Impostazioni SET Valore = 'SI' WHERE Impostazione = 'Umid_Notify';";
            $connection->exec($sql);
        }
        else 
        {
            $sql = "UPDATE Impostazioni SET Valore = 'NO' WHERE Impostazione = 'Umid_Notify';";
            $connection->exec($sql);
        }

        if(isset($_POST["Press"]))
        {
            $sql = "UPDATE Impostazioni SET Valore = 'SI' WHERE Impostazione = 'Pres_Notify';";
            $connection->exec($sql);
        }
        else 
        {
            $sql = "UPDATE Impostazioni SET Valore = 'NO' WHERE Impostazione = 'Pres_Notify';";
            $connection->exec($sql);
        }

        if(isset($_POST["CO2"]))
        {
            $sql = "UPDATE Impostazioni SET Valore = 'SI' WHERE Impostazione = 'CO2_Notify';";
            $connection->exec($sql);
        }
        else 
        {
            $sql = "UPDATE Impostazioni SET Valore = 'NO' WHERE Impostazione = 'CO2_Notify';";
            $connection->exec($sql);
        }
        header("location: /impostazioni/notifiche.php");
    }
?>