<?php
    class check
    {
        private $temp = "";
        private $umid = "";
        private $press = "";
        private $co2 = "";
        //Offset per avere un certo margine prima di inviare una notifica
        private $offset_temperatura = 1;
        private $offset_umidita = 2;

        function __construct($temp,$umid,$press,$co2) 
        {
            $this->temp = $temp;
            $this->umid = $umid;
            $this->press = $press;
            $this->co2 = $co2;
        }

        function checkAll()
        {
            if(date("G") == 0)//Azione eseguita solo a mezzanotte
            {
                $this->updateSettingsAuto();
            }
            $result = array();
            $result["Temp"] = $this->SendNotificationTemp();
            $result["Umid"] = $this->SendNotificationUmid();
            $result["Press"] = $this->SendNotificationPress();
            $result["CO2"] = $this->SendNotificationCO2();
            if($result["Temp"] !="" && $result["Umid"] !="" && $result["Press"] !="" && $result["CO2"] !="")
            {
                $message = "La stazione meteo potrebbe non funzionare correttamente";
                $this->sendMessage("AirHome errore stazione",$message);
            }
            else
            {
                $fatto = 0;
                if($result["CO2"] !="")
                {
                    $fatto=1;
                    $this->sendMessage("AirHome CO2",$result["CO2"]);
                }
                if($result["Temp"] !="" && $fatto == 0)
                {
                    $fatto=1;
                    $this->sendMessage("AirHome Temperatura",$result["Temp"]);
                }
                if($result["Umid"] !="" && $fatto == 0)
                {
                    $fatto=1;
                    $this->sendMessage("AirHome Umidità",$result["Umid"]);
                }
                if($result["Press"] !="" && $fatto == 0)
                {
                    $fatto=1;
                    $this->sendMessage("AirHome Pressione",$result["Press"]);
                }
            }
        }
        function sendMessage($title,$message)
        {
            require($_SERVER['DOCUMENT_ROOT'] ."/php/settings.php");

            
            $NightMode = false;
            //Controllo se la modalità notte è attiva
            if($connection->query("SELECT Valore FROM Impostazioni WHERE Impostazione = 'Nightmode';")->fetch()["Valore"]=="OK")
            {
                $h = date("G");
                if($h>=22 || $h<6)
                {
                    $NightMode = true;
                }
            }

            if(!$NightMode)
            {
                //Controllo se il device è in silenzioso
                $DeviceNotifyUrlArr= array();
                $adesso = strtotime(date("Y-m-d H:i:00"));
                foreach($connection->query('SELECT * FROM NotificheUWP') as $row) 
                {
                    $silenzioso = strtotime($row["silenzioso"]);
                    $minuti = round(($adesso - $silenzioso) / 60,2);
                    if($minuti>=0)
                    {
                        $DeviceNotifyUrlArr[] = array($row['URL'],"OK");
                    }
                    else 
                    {
                        $DeviceNotifyUrlArr[] = array($row['URL'],"KO");
                    }
                }

                require($_SERVER['DOCUMENT_ROOT'] ."/php/insert/wns.php");
                $wns= new Wns();//Create Wns class object
                $xml_string = $wns->buildTileXml($title, $message);
                for($i=0; $i< count($DeviceNotifyUrlArr);$i++)
                {
                    if($DeviceNotifyUrlArr[$i][1] == "OK")
                    {
                        $response = $wns->sendWindowsNotification($DeviceNotifyUrlArr[$i][0], $xml_string);
                        $response->ToString();
                    }
                }
            }
        }
        
        function updateSettingsAuto($saltagiorno=0)
        {
            require($_SERVER['DOCUMENT_ROOT'] ."/php/settings.php");
            //TEMP
            $result = $connection->query("SELECT Valore,DAY(Modifica) AS 'DAY' FROM personalRange WHERE Impostazione = 'Temperatura_max';")->fetch();
            if(($result["DAY"] != date("j")) || $saltagiorno == 1)//se passo da manuale ad auto, voglio che il dato si aggiorni senza guardare il giorno
            {
                if($result["Valore"] == "Auto")
                {
                    $r = $this->UPDATE("Temperatura","1");//MAX TEMP
                    $date = date("Y-m-d");
                    $sql = "UPDATE personalRange SET Valore_AUTO = '$r' WHERE Impostazione ='Temperatura_max';
                            UPDATE personalRange SET Modifica = '$date' WHERE Impostazione ='Temperatura_max';";
                    $connection->exec($sql);
                    echo "Temp MAX modificata<br>";
                }
            }
            $result = $connection->query("SELECT Valore,DAY(Modifica) AS 'DAY' FROM personalRange WHERE Impostazione = 'Temperatura_min';")->fetch();
            if($result["DAY"] != date("j") || $saltagiorno == 1)
            {
                if($result["Valore"] == "Auto")
                {
                    $r = $this->UPDATE("Temperatura","0");//MIN TEMP
                    $date = date("Y-m-d");
                    $sql = "UPDATE personalRange SET Valore_AUTO = '$r' WHERE Impostazione ='Temperatura_min';
                            UPDATE personalRange SET Modifica = '$date' WHERE Impostazione ='Temperatura_min';";
                    $connection->exec($sql);
                    echo "Temp MIN modificata<br>";
                }
            }
            //UMID
            $result = $connection->query("SELECT Valore,DAY(Modifica) AS 'DAY' FROM personalRange WHERE Impostazione = 'Umidita_max';")->fetch();
            if($result["DAY"] != date("j") || $saltagiorno == 1)
            {
                if($result["Valore"] == "Auto")
                {
                    $r = $this->UPDATE("Umidita","1");//MAX UMID
                    $date = date("Y-m-d");
                    $sql = "UPDATE personalRange SET Valore_AUTO = '$r' WHERE Impostazione ='Umidita_max';
                            UPDATE personalRange SET Modifica = '$date' WHERE Impostazione ='Umidita_max';";
                    $connection->exec($sql);
                    echo "Umid MAX modificata<br>";
                }
            }
            $result = $connection->query("SELECT Valore,DAY(Modifica) AS 'DAY' FROM personalRange WHERE Impostazione = 'Umidita_min';")->fetch();
            if($result["DAY"] != date("j") || $saltagiorno == 1)
            {
                if($result["Valore"] == "Auto")
                {
                    $r = $this->UPDATE("Umidita","0");//MIN UMID
                    $date = date("Y-m-d");
                    $sql = "UPDATE personalRange SET Valore_AUTO = '$r' WHERE Impostazione ='Umidita_min';
                            UPDATE personalRange SET Modifica = '$date' WHERE Impostazione ='Umidita_min';";
                    $connection->exec($sql);
                    echo "Umid MIN modificata<br>";
                }
            }
            //PRESS
            $result = $connection->query("SELECT Valore,DAY(Modifica) AS 'DAY' FROM personalRange WHERE Impostazione = 'Press_max';")->fetch();
            if($result["DAY"] != date("j") || $saltagiorno == 1)
            {
                if($result["Valore"] == "Auto")
                {
                    $r = $this->UPDATE("Pressione","1");//MAX PRESS
                    $date = date("Y-m-d");
                    $sql = "UPDATE personalRange SET Valore_AUTO = '$r' WHERE Impostazione ='Press_max';
                            UPDATE personalRange SET Modifica = '$date' WHERE Impostazione ='Press_max';";
                    $connection->exec($sql);
                    echo "Press MAX modificata<br>";
                }
            }
            $result = $connection->query("SELECT Valore,DAY(Modifica) AS 'DAY' FROM personalRange WHERE Impostazione = 'Press_min';")->fetch();
            if($result["DAY"] != date("j") || $saltagiorno == 1)
            {
                if($result["Valore"] == "Auto")
                {
                    $r = $this->UPDATE("Pressione","0");//MIN PRESS
                    $date = date("Y-m-d");
                    $sql = "UPDATE personalRange SET Valore_AUTO = '$r' WHERE Impostazione ='Press_min';
                            UPDATE personalRange SET Modifica = '$date' WHERE Impostazione ='Press_min';";
                    $connection->exec($sql);
                    echo "Press MIN modificata<br>";
                }
            }
            $connection = NULL;
        }

        function UPDATE($Colonna,$max)
        {
            require($_SERVER['DOCUMENT_ROOT'] ."/php/settings.php");
            if($max == "1")
            {
                $result = $connection->query("SELECT TRUNCATE(MAX($Colonna),2) AS 'RES' FROM Misurazioni WHERE DATEDIFF((SELECT MAX(Ora) FROM Misurazioni), Ora) < 3;");
            }
            if($max == "0")
            {
                $result = $connection->query("SELECT TRUNCATE(MIN($Colonna),2) AS 'RES' FROM Misurazioni WHERE DATEDIFF((SELECT MAX(Ora) FROM Misurazioni), Ora) < 3;");
            }
            return $result->fetch()["RES"];
        }

        function SendNotificationTemp()
        {
            require($_SERVER['DOCUMENT_ROOT'] ."/php/settings.php");
            $result = $connection->query("SELECT Valore,UltimoInvio FROM Impostazioni WHERE Impostazione = 'Temp_Notify';")->fetch();
            $adesso = strtotime(date("Y-m-d H:i:00"));
            $silenzioso = strtotime($result["UltimoInvio"]);
            $interval = round(($adesso - $silenzioso) / 60,2);
            $adesso=date("Y-m-d H:i:00");
            
            if($result["Valore"]=="SI" && $interval >= 60)
            {
                $min="";
                $max="";
                $invio_max;
                $invio_min;

                foreach($connection->query("SELECT * FROM personalRange WHERE Impostazione = 'Temperatura_min' || Impostazione = 'Temperatura_max';") as $row) 
                {
                    if(strpos($row['Impostazione'],"max"))
                    {
                        if($row['Valore']=="Auto")
                            $max=$row['Valore_AUTO'];
                        else 
                            $max=$row['Valore'];
                    }
                    else 
                    {
                        if($row['Valore']=="Auto")
                            $min=$row['Valore_AUTO'];
                        else
                            $min=$row['Valore'];
                    }
                }
                if($this->temp > $max)
                {
                    $sql = "UPDATE Impostazioni SET UltimoInvio = '$adesso' WHERE Impostazione ='Temp_Notify';";
                    $connection->exec($sql);
                    return "Temperatura rilevata superiore al limite!";
                }

                if($this->temp < $min)
                {
                    $sql = "UPDATE Impostazioni SET UltimoInvio = '$adesso' WHERE Impostazione ='Temp_Notify';";
                    $connection->exec($sql);
                    return "Temperatura rilevata inferiore al limite!";
                }
            }     
            else 
            {
                return "";
            }
            
        }

        function SendNotificationUmid()
        {
            require($_SERVER['DOCUMENT_ROOT'] ."/php/settings.php");
            //Prendo le ultime 20 umidita
            $result = $connection->query("SELECT Valore,UltimoInvio FROM Impostazioni WHERE Impostazione = 'Umid_Notify';")->fetch();
            $adesso = strtotime(date("Y-m-d H:i:00"));
            $silenzioso = strtotime($result["UltimoInvio"]);
            $interval = round(($adesso - $silenzioso) / 60,2);
            $adesso=date("Y-m-d H:i:00");

            if($result["Valore"]=="SI" && $interval >= 60)
            {
                $ultimaH = array();
                foreach($connection->query("SELECT Umidita FROM Misurazioni WHERE ID > ((SELECT MAX(ID) FROM Misurazioni)-12);") as $row) 
                {
                    $ultimaH[] = $row['Umidita'];
                }

                //se ho dati a sufficienza calcolo la media
                if(count($ultimaH)==12)
                {
                    $media = array_sum($ultimaH)/count($ultimaH);
                    $min;
                    $max;
                    foreach($connection->query("SELECT * FROM personalRange WHERE Impostazione = 'Umidita_min' || Impostazione = 'Umidita_max';") as $row) 
                    {
                        if(strpos($row['Impostazione'],"max"))
                        {
                            if($row['Valore']=="Auto")
                                $max=$row['Valore_AUTO'];
                            else 
                                $max=$row['Valore'];
                        }
                        else 
                        {
                            if($row['Valore']=="Auto")
                                $min=$row['Valore_AUTO'];
                            else
                                $min=$row['Valore'];
                        }
                    }
                    if($this->umid > $max)
                    {
                        $sql = "UPDATE Impostazioni SET UltimoInvio = '$adesso' WHERE Impostazione ='Umid_Notify';";
                        $connection->exec($sql);
                        return "Umidità rilevata superiore al limite!";
                    }
                    if($this->umid < $min)
                    {
                        $sql = "UPDATE Impostazioni SET UltimoInvio = '$adesso' WHERE Impostazione ='Umid_Notify';";
                        $connection->exec($sql);
                        return "Umidità rilevata inferiore al limite!";
                    }

                    if($this->umid > $media + $this->offset_umidita)
                    {
                        $sql = "UPDATE Impostazioni SET UltimoInvio = '$adesso' WHERE Impostazione ='Umid_Notify';";
                        $connection->exec($sql);
                        return "Umidità in aumento nell'ultima ora";
                    }

                    if($this->umid < $media - $this->offset_umidita)
                    {
                        $sql = "UPDATE Impostazioni SET UltimoInvio = '$adesso' WHERE Impostazione ='Umid_Notify';";
                        $connection->exec($sql);
                        return "Umidità in calo nell'ultima ora";
                    }
                }
            }
            else
            {
                return "";
            }
        }

        function SendNotificationPress()
        {
            require($_SERVER['DOCUMENT_ROOT'] ."/php/settings.php");
            $result = $connection->query("SELECT Valore,UltimoInvio FROM Impostazioni WHERE Impostazione = 'Pres_Notify';")->fetch();
            $adesso = strtotime(date("Y-m-d H:i:00"));
            $silenzioso = strtotime($result["UltimoInvio"]);
            $interval = round(($adesso - $silenzioso) / 60,2);
            $adesso=date("Y-m-d H:i:00");

            if($result["Valore"]=="SI" && $interval >= 60)
            {
                //Prendo le ultime 20 pressioni
                $ultimaH = array();
                foreach($connection->query("SELECT Pressione FROM Misurazioni WHERE ID > ((SELECT MAX(ID) FROM Misurazioni)-12);") as $row) 
                {
                    $ultimaH[] = $row['Pressione'];
                }

                //se ho dati a sufficienza calcolo la media
                if(count($ultimaH)==12)
                {
                    $media = array_sum($ultimaH)/count($ultimaH);
                    $min;
                    $max;
                    foreach($connection->query("SELECT * FROM personalRange WHERE Impostazione = 'Press_min' || Impostazione = 'Press_max';") as $row) 
                    {
                        if(strpos($row['Impostazione'],"max"))
                        {
                            if($row['Valore']=="Auto")
                                $max=$row['Valore_AUTO'];
                            else 
                                $max=$row['Valore'];
                        }
                        else 
                        {
                            if($row['Valore']=="Auto")
                                $min=$row['Valore_AUTO'];
                            else
                                $min=$row['Valore'];
                        }
                    }
                    if($this->press > $max)
                    {
                        $sql = "UPDATE Impostazioni SET UltimoInvio = '$adesso' WHERE Impostazione ='Pres_Notify';";
                        $connection->exec($sql);
                        return "Pressione rilevata superiore al limite!";
                    }
                    if($this->press < $min)
                    {
                        $sql = "UPDATE Impostazioni SET UltimoInvio = '$adesso' WHERE Impostazione ='Pres_Notify';";
                        $connection->exec($sql);
                        return "Pressione rilevata inferiore al limite!";
                    }

                    if($this->press > $media)
                    {
                        $sql = "UPDATE Impostazioni SET UltimoInvio = '$adesso' WHERE Impostazione ='Pres_Notify';";
                        $connection->exec($sql);
                        return "Pressione in aumento nell'ultima ora";
                    }

                    if($this->press < $media)
                    {
                        $sql = "UPDATE Impostazioni SET UltimoInvio = '$adesso' WHERE Impostazione ='Pres_Notify';";
                        $connection->exec($sql);
                        return "Pressione in calo nell'ultima ora";
                    }      
                }
            }
            else
            {
                return "";
            }
        }

        function SendNotificationCO2()
        {
            require($_SERVER['DOCUMENT_ROOT'] ."/php/settings.php");
            $result = $connection->query("SELECT Valore,UltimoInvio FROM Impostazioni WHERE Impostazione = 'CO2_Notify';")->fetch();
            $adesso = strtotime(date("Y-m-d H:i:00"));
            $silenzioso = strtotime($result["UltimoInvio"]);
            $interval = round(($adesso - $silenzioso) / 60,2);
            $adesso=date("Y-m-d H:i:00");

            if($result["Valore"]=="SI" && $interval >= 60)
            {
                if($this->co2 >= 1500)
                {
                    $sql = "UPDATE Impostazioni SET UltimoInvio = '$adesso' WHERE Impostazione ='CO2_Notify';";
                    $connection->exec($sql);
                    return "PERICOLO: Aria molto contaminata (ppm:$this->co2), arieggia immediatamente!";
                }
                else if($this->co2 >= 1100)
                {
                    $sql = "UPDATE Impostazioni SET UltimoInvio = '$adesso' WHERE Impostazione ='CO2_Notify';";
                    $connection->exec($sql);
                    return "Aria contaminata (ppm: $this->co2), arieggia la stanza!";
                }
                else if($this->co2 >= 700)
                {
                    $sql = "UPDATE Impostazioni SET UltimoInvio = '$adesso' WHERE Impostazione ='CO2_Notify';";
                    $connection->exec($sql);
                    return "Se riesci, apri per 30 minuti la finestra (ppm: $this->co2)";
                }
                else 
                {
                    return "";
                }
            }
            else 
            {
                return "";
            }
        }
        
        function ToString()
        {
            echo "Temperatura: " . $this->temp .
            "<br>Umidita': " . $this->umid.
            "<br>Pressione: " . $this->press.
            "<br>CO2: " . $this->co2;
        }
    }
?>