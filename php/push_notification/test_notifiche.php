<?php
    if(isset($_GET["mex"]))
    {
        require($_SERVER['DOCUMENT_ROOT'] ."/php/settings.php");
        $DeviceNotifyUrlArr= array();
        $adesso = strtotime(date("Y-m-d H:i:00"));

        foreach($connection->query("SELECT * FROM NotificheUWP") as $row) 
        {
            $silenzioso = strtotime($row["silenzioso"]);
            $minuti = round(($adesso - $silenzioso) / 60,2);
            if($minuti>=0)
            {
                $DeviceNotifyUrlArr[] = array($row['URL'],"OK");
            }
            else 
            {
                $DeviceNotifyUrlArr[] = array($row['URL'],"KO");
            }
        }

        require($_SERVER['DOCUMENT_ROOT'] ."/php/insert/wns.php");
        $wns= new Wns();//Create Wns class object
        $title = "AirHome Test";
        $message= $_GET["mex"];
        $xml_string = $wns->buildTileXml($title, $message);
        header('Content-Type: text/xml');
        echo "<?xml version=\"1.0\" ?>\n";
        echo "<notifiche>";
        for($i=0; $i < count($DeviceNotifyUrlArr);$i++)
        {
            $s = $i;
            $s++;
            echo "<notifica>" . $s . " ";
            if($DeviceNotifyUrlArr[$i][1] == "OK")
            {
                $response = $wns->sendWindowsNotification($DeviceNotifyUrlArr[$i][0], $xml_string);
                $response->ToString();
            }
            else 
            {
                echo"The device is on silent mode";
            }
            echo"</notifica>";
        }
        echo"</notifiche>";
    }
    
?>