<?php
    if(isset($_GET["key"]))
    {
        if($_GET["key"] == "9c526a3db470c2233929169c69a2c07319f7b868")
        {
            require($_SERVER['DOCUMENT_ROOT'] ."/php/settings.php");
            require($_SERVER['DOCUMENT_ROOT'] ."/php/weather.php");
            $result = $connection->query("SELECT Temperatura,Umidita,CO2,TIME(Ora) AS Ora FROM Misurazioni WHERE ID = (SELECT MAX(ID) FROM Misurazioni);")->fetch(); 
            $temp = $result["Temperatura"];
            $umid = $result["Umidita"];
            $co2 = $result["CO2"];
            $ora = $result["Ora"];
            $meteo = new MeteoEsterno();
            $tempExt = $meteo->temp();
            header("Content-Type:text/xml");
            echo "<tile>
                    <visual branding=\"name\">
                        <binding template=\"TileSmall\" hint-textStacking=\"center\">
                            <text hint-wrap=\"true\" hint-style=\"base\" hint-align=\"center\">" . $temp . "°C</text>
                        </binding>
                        <binding template=\"TileMedium\">
                            <text hint-wrap=\"true\" hint-style=\"base\" hint-align=\"center\">" . $temp . "°C</text>
                            <text hint-wrap=\"true\" hint-style=\"base\" hint-align=\"center\">" . $umid . "%</text>
                            <text hint-wrap=\"true\" hint-style=\"base\" hint-align=\"center\">" . $co2 . " ppm</text>
                            <text hint-wrap=\"true\" hint-style=\"captionSubtle\" hint-align=\"center\">Aggiornamento: $ora</text>
                        </binding>
                        
                        <binding template=\"TileWide\">
                            <text hint-wrap=\"true\" hint-style=\"base\">Temperatura interna: " . $temp . "°C</text>
                            <text hint-wrap=\"true\" hint-style=\"base\">Temperatura esterna: " . $tempExt . "</text>
                            <text hint-wrap=\"true\" hint-style=\"base\">Umidità: " . $umid . "%</text>
                            <text hint-wrap=\"true\" hint-style=\"base\">CO2: " . $co2 . " ppm</text>
                            <text hint-wrap=\"true\" hint-style=\"captionSubtle\">Aggiornamento: $ora</text>
                        </binding>

                        <binding template=\"TileLarge\">
                            <text hint-wrap=\"true\" hint-style=\"base\">Temperatura interna: " . $temp . "°C</text>
                            <text hint-wrap=\"true\" hint-style=\"base\">Temperatura esterna: " . $tempExt . "</text>
                            <text hint-wrap=\"true\" hint-style=\"base\">Umidità: " . $umid . "%</text>
                            <text hint-wrap=\"true\" hint-style=\"base\">CO2: " . $co2 . " ppm</text>
                            <text hint-wrap=\"true\" hint-style=\"captionSubtle\">Aggiornamento: $ora</text>
                        </binding>
                    </visual>
                </tile>";
        }
    }
?>