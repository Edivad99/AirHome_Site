<?php
class FindIdByName{
    function __construct(){
    }

    function citta($paese)
    {
        $url = "https://www.ilmeteo.net/peticionBuscador.php?lang=it&texto=" . $paese; // path to your JSON file
        $data = file_get_contents($url); // put the contents of the file into a variables
        $characters = json_decode($data); // decode the JSON feed
        $id = -1;
        for($i = 0; $i < count($characters->localidad);$i++)
        {
            if($characters->localidad[$i]->nombre == $paese)
            {
                $id = $characters->localidad[$i]->id;
            }
                
        }
        return $id;
    }

}
class MeteoEsterno{
    public $characters = '';
    private $interval = '';
    private $hour;

    function __construct(){
        require($_SERVER['DOCUMENT_ROOT'] ."/php/settings.php");
        $result = $connection->query("SELECT Valore FROM Impostazioni WHERE Impostazione = 'ComuneValore';")->fetch();
        $id = $result["Valore"];
        $url = "http://api.ilmeteo.net/index.php?api_lang=it&localidad=$id&affiliate_id=tv5fzjx35c8v&v=3.0"; // path to your JSON file
        $data = file_get_contents($url); // put the contents of the file into a variables
        $this->characters = json_decode($data); // decode the JSON feed

        $ora = date("H");
        $this->hour = $ora;
        if($ora>=23)
            $this->interval = 7;
        else if ($ora>=20)
            $this->interval = 6;
        else if ($ora>=17)
            $this->interval = 5;
        else if ($ora>=14)
            $this->interval = 4;
        else if ($ora>=11)
            $this->interval = 3;
        else if ($ora>=8)
            $this->interval = 2;
        else if ($ora>=5)
            $this->interval = 1;
        else
            $this->interval = 0;
    }

    function citta()
    {
        require($_SERVER['DOCUMENT_ROOT'] ."/php/settings.php");
        $result = $connection->query("SELECT Valore FROM Impostazioni WHERE Impostazione = 'Comune';")->fetch();
        return $result["Valore"];
    }


    function temp()
    {
        $temp = $this->characters->day[0]->hour[$this->interval]->temp;
        $unit = $this->characters->day[0]->units->temp;
        return $temp. $unit;
    }

    function tempMin()
    {
        $ora = date("H");
        $temp = $this->characters->day[0]->tempmin;
        $unit = $this->characters->day[0]->units->temp;
        return $temp. $unit;
    } 

    function tempMax()
    {
        $temp = $this->characters->day[0]->tempmax;
        $unit = $this->characters->day[0]->units->temp;
        return $temp. $unit;
    }

    function getStatusImage()
    {
        return $this->characters->day[0]->hour[$this->interval]->symbol_value;
    }

    function humidity()
    {
        $umid =  $this->characters->day[0]->hour[$this->interval]->humidity;
        $unit = "%";
        return $umid. $unit;
    }

    function pressure()
    {
        $pressure =  $this->characters->day[0]->hour[$this->interval]->pressure;
        //1mBar = 100 Pa
        $pressure = $pressure * 100;
        return $pressure. " " . "Pa";
    }

    function pioggia()
    {
        $rain = $this->characters->day[0]->hour[$this->interval]->rain;
        $unit = $this->characters->day[0]->units->rain;
        return $rain . " " . $unit;
    }

    function nuvole()
    {
        return $this->characters->day[0]->hour[$this->interval]->clouds;
    }

    function alba()
    {
        return $this->characters->day[0]->sun->in;
    }

    function tramonto()
    {
        return $this->characters->day[0]->sun->out;
    }

    function luna($img = 0)
    {
        if($img == 1)
        {
            return $this->characters->day[0]->moon->symbol;
        }
        else
        {
            return $this->characters->day[0]->moon->desc;
        }
        
    }

    function vento($img = 0)
    {
        if($img == 1)
        {
            return $this->characters->day[0]->hour[$this->interval]->wind->symbol;
        }
        else if ($img ==2)
        {
            return $this->characters->day[0]->hour[$this->interval]->wind->dir;
        }
        else
        {
            $wind =  $this->characters->day[0]->hour[$this->interval]->wind->speed;
            $unit = $this->characters->day[0]->units->wind;
            return $wind . " " . $unit;
        }
        
    }

    function descrizione()
    {
        return $this->characters->day[0]->hour[$this->interval]->symbol_description;
    }
    function lastUpdate()
    {
        return $this->characters->day[0]->hour[$this->interval]->interval;
    }
    function Json()
    {
        $s = 
        "[{\"Citta\":\"" . $this->citta()."\",".
        "\"Temperatura_Minima\":\"" . $this->tempMin()."\",".
        "\"Temperatura_Massima\":\"" . $this->tempMax()."\",".
        "\"Temperatura\":\"" . $this->temp()."\",".
        "\"Umidita\":\"" . $this->humidity()."\",".
        "\"Pioggia\":\"" . $this->pioggia()."\",".
        "\"Nuvole\":\"" . $this->nuvole()."\",".
        "\"Pressione\":\"" . $this->pressure()."\",".
        "\"Alba\":\"" . $this->alba()."\",".
        "\"Tramonto\":\"" . $this->tramonto()."\",".
        "\"Luna\":\"" . $this->luna()."\",".
        "\"Luna_immagine\":\"" . $this->luna(1)."\",".
        "\"Vento\":\"" . $this->vento()."\",".
        "\"Vento_immagine\":\"" . $this->vento(1) ."\",".
        "\"Vento_direzione\":\"" . $this->vento(2) ."\",".
        "\"Condizione_immagine\":\"" . $this->getStatusImage() . "\"," .
        "\"Descrizione\":\"" . $this->descrizione() . "\",".
        "\"Last_Update\":\"" . $this->lastUpdate() ."\"}]";
        echo $s;
    }
    function ToString()
    {
        echo 
        "Citta: " . $this->citta()."<br>".
        "Temperatura Minima: " . $this->tempMin()."<br>".
        "Temperatura Massima: " . $this->tempMax()."<br>".
        "Temperatura: " . $this->temp()."<br>".
        "Umidita: " . $this->humidity()."<br>".
        "Pioggia: " . $this->pioggia()."<br>".
        "Nuvole: " . $this->nuvole()."<br>".
        "Pressione: " . $this->pressure()."<br>".
        "Alba: " . $this->alba()."<br>".
        "Tramonto: " . $this->tramonto()."<br>".
        "Luna: " . $this->luna()."<br>".
        "Luna immagine: " . $this->luna(1)."<br>".
        "Vento: " . $this->vento()."<br>".
        "Vento immagine: " . $this->vento(1) ."<br>".
        "Vento direzione: " . $this->vento(2) ."<br>".
        "Condizione immagine: " . $this->getStatusImage() . "<br>" .
        "Descrizione: " . $this->descrizione() . "<br>".
        "Last Update: " . $this->lastUpdate() . "<br>".
        "Ora: " . $this->hour . "<br>".
        "Intervallo: " . $this->interval ."<br>";
    }
}
?>