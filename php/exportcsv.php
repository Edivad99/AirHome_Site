<?php
    session_start();
    if(!isset($_SESSION["login"]) || $_SESSION["login"] != "OK")
    {
        header("location: /index.php");
    }

    if(isset($_GET['giorno']))
    {
        /*Recupero data e ora corrente*/
        $giorno = str_replace("/","-",$_GET['giorno']);
        $ora  = date("H.i.s");
        $path = $_SERVER['DOCUMENT_ROOT'] . "/data/Esportazione_$giorno" . "_$ora.csv";
        $myfile = fopen($path, "w") or print_r("Unable to open file\nError: " . error_get_last());
        require($_SERVER['DOCUMENT_ROOT'] ."/php/settings.php");

        $sql = "SELECT TIME(Ora) AS 'Ora',Temperatura,Umidita,Pressione,CO2 FROM Misurazioni WHERE DATE(Ora) = '$giorno'";
        $result = $connection->query($sql);
        $data = "Giorno:;" . $giorno . "\n" . "Ora;Temperatura;Umidita';Pressione;CO2\n";
        while($row = $result->fetch(PDO::FETCH_ASSOC))
		{
            $data.= $row['Ora'] . ";" . $row['Temperatura'] . ";" . $row['Umidita'] . ";" . $row['Pressione'] . ";" . $row['CO2'] . "\n"; 
			$dataArray[$row['Ora']]=$row['Temperatura'];
		}
        $data = str_replace(".",",",$data);
        fwrite($myfile, $data);
        fclose($myfile);
        // set the download rate limit (=> 20,5 kb/s)
        $download_rate = 20.5;
        if(file_exists($path) && is_file($path))
        {
            header('Cache-control: private');
            header('Content-Type: application/octet-stream');
            header('Content-Length: '.filesize($path));
            header('Content-Disposition: filename=Esportazione_' . $giorno . '_' . $ora . '.csv');

            flush();
            $file = fopen($path, "r");
            while(!feof($file))
            {
                // send the current file part to the browser
                print fread($file, round($download_rate * 1024));
                // flush the content to the browser
                flush();
                // sleep one second
                sleep(1);
            }
            fclose($file);
            unlink($path);
        }
        else
        {
            die('Error: The file '.$path.' does not exist!');
        }
    }
?>
