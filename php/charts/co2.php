<?php
	require($_SERVER['DOCUMENT_ROOT'] ."/php/phpgraphlib7.0.php");
	$graph=new PHPGraphLib(850,450);

	require($_SERVER['DOCUMENT_ROOT'] ."/php/settings.php");
	$cerca = false;
	if(isset($_GET["data"]) && $_GET["data"] != "")
	{
		$giorno = $_GET["data"];
		$result = $connection->query("SELECT TIME_FORMAT(Ora, '%H') AS Ora,ROUND(AVG(CO2),0) AS 'CO2' FROM Misurazioni WHERE DATE(Ora) = '$giorno' GROUP BY (HOUR(ora));");	
		$cerca = true;
	}
	else 
	{
		$result = $connection->query("SELECT CO2, TIME_FORMAT(Ora, '%H.%i') AS Ora FROM Misurazioni WHERE ID > ((SELECT MAX(ID) FROM Misurazioni)-20) AND DATE(Ora) = (SELECT DATE(MAX(Ora)) FROM Misurazioni);");
	}
	if($result)
	{
		$dataArray = array();
		while($row = $result->fetch(PDO::FETCH_ASSOC))
		{
			$dataArray[$row['Ora']]=$row['CO2'];
        }
        $graph->setLogarithmic(true);
		$graph->addData($dataArray);
		$graph->setBackgroundColor("#76BF72");
		$graph->setTextColor('white');
		$graph->setBars(false);
		$graph->setLine(true);
		$graph->setDataPoints(true);
		$graph->setDataPointColor('teal');
		$graph->setDataValues(true);
		$graph->setDataValueColor('black');
		$graph->setXValuesHorizontal($cerca);//imposta se la x è orrizontale o no

		if($cerca)
		{
			$media = $connection->query("SELECT ROUND(AVG(CO2),2) AS 'CO2' FROM Misurazioni WHERE DATE(Ora) = '$giorno';")->fetch()['CO2'];
		}
		else 
		{
			$media = $connection->query("SELECT ROUND(AVG(CO2),2) AS 'CO2' FROM Misurazioni WHERE DATE(Ora) = (SELECT DATE(MAX(Ora)) FROM Misurazioni);")->fetch()['CO2'];
		}

		$graph->setGoalLine($media,'blue','solid');
        $graph->createGraph();
        
	}
?>