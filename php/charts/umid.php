<?php
	require($_SERVER['DOCUMENT_ROOT'] ."/php/phpgraphlib7.0.php");
	$graph=new PHPGraphLib(850,450);

	require($_SERVER['DOCUMENT_ROOT'] ."/php/settings.php");
	$cerca = false;
	if(isset($_GET["data"]) && $_GET["data"] != "")
	{
		$giorno = $_GET["data"];
		$result = $connection->query("SELECT TIME_FORMAT(Ora, '%H') AS Ora,ROUND(AVG(Umidita),2) AS 'Umidita' FROM Misurazioni WHERE DATE(Ora) = '$giorno' GROUP BY (HOUR(ora));");	
		$cerca = true;
	}
	else 
	{
		$result = $connection->query("SELECT Umidita, TIME_FORMAT(Ora, '%H.%i') AS Ora FROM Misurazioni WHERE ID > ((SELECT MAX(ID) FROM Misurazioni)-20) AND DATE(Ora) = (SELECT DATE(MAX(Ora)) FROM Misurazioni);");
	}

	if($result)
	{
		$dataArray = array();
		while($row = $result->fetch(PDO::FETCH_ASSOC))
		{
			$dataArray[$row['Ora']]=$row['Umidita'];
		}
		$graph->setLogarithmic(true);
		$graph->addData($dataArray);
		$graph->setBackgroundColor("#76BF72");
		$graph->setTextColor('white');
		$graph->setBars(false);
		$graph->setLine(true);
		$graph->setDataPoints(true);
		$graph->setDataPointColor('red');
		$graph->setDataValues(true);
		$graph->setDataValueColor('black');
		$graph->setXValuesHorizontal($cerca);//imposta se la x è orrizontale o no

		if($cerca)
		{
			$media = $connection->query("SELECT ROUND(AVG(Umidita),2) AS 'Umidita' FROM Misurazioni WHERE DATE(Ora) = '$giorno';")->fetch()['Umidita'];
		}
		else 
		{
			$media = $connection->query("SELECT ROUND(AVG(Umidita),2) AS 'Umidita' FROM Misurazioni WHERE DATE(Ora) = (SELECT DATE(MAX(Ora)) FROM Misurazioni);")->fetch()['Umidita'];
		}

		$graph->setGoalLine($media,'blue','solid');
		$graph->createGraph();
	}
?>