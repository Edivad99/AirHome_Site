<?php
if(isset($_GET['key']) && isset($_GET['qr']))
{
  $key = $_GET['key'];
  $mykey="9c526a3db470c2233929169c69a2c07319f7b868";
  if($key == $mykey && is_numeric($_GET['qr']))
  {
    $qr = $_GET['qr'];
    $date;
    if($qr == 101 && isset($_GET['date']))
    {
      $date = $_GET['date'];
    }

    $table ="Misurazioni";
    if($qr == 0)
    {
      $qr = "SELECT Temperatura,Umidita,Pressione,CO2,Ora FROM $table WHERE ID > ((SELECT MAX(ID) FROM $table)-20);";
    }
    else if($qr == 1)
    {
      $qr = "SELECT Temperatura,Ora FROM $table WHERE ID > ((SELECT MAX(ID) FROM $table)-20);";
    }
    else if($qr == 2)
    {
      $qr = "SELECT Umidita,Ora FROM $table WHERE ID > ((SELECT MAX(ID) FROM $table)-20);";
    }
    else if($qr == 3)
    {
      $qr = "SELECT Pressione,Ora FROM $table WHERE ID > ((SELECT MAX(ID) FROM $table)-20);";
    }
    else if($qr == 4)
    {
      $qr = "SELECT CO2,Ora FROM $table WHERE ID > ((SELECT MAX(ID) FROM $table)-20);";
    }
    else if($qr == 5)
    {
      $qr = "SELECT TRUNCATE(MIN(Temperatura), 2) AS 'Temp_min', TRUNCATE(MAX(Temperatura), 2) AS 'Temp_max', TRUNCATE(MIN(Umidita), 2) AS 'Umid_min', TRUNCATE(MAX(Umidita), 2) AS 'Umid_max', TRUNCATE(MIN(Pressione), 2) AS 'Pres_min', TRUNCATE(MAX(Pressione), 2) AS 'Pres_max', TRUNCATE(MIN(CO2), 2) AS 'CO2_min', TRUNCATE(MAX(CO2), 2) AS 'CO2_max',DATE(Ora) AS 'Ora' FROM $table WHERE DATE(Ora) = ((SELECT MAX(DATE(Ora)) FROM $table));";
    }
    else if($qr == 6)
    {
      $qr = "SELECT TRUNCATE(MIN(Temperatura), 2) AS 'Temp_min', TRUNCATE(MAX(Temperatura), 2) AS 'Temp_max', TRUNCATE(AVG(Temperatura), 2) AS 'Temp_media',DATE(Ora) AS 'Ora', (SELECT TRUNCATE(AVG(Temperatura),2) FROM $table WHERE DATE(Ora) = DATE_SUB((SELECT DATE(MAX(Ora)) FROM $table), INTERVAL 1 DAY)) AS 'Temp_media_precedente', DATE_SUB(DATE(Ora),INTERVAL 1 DAY) AS 'Ora_precedente' FROM $table WHERE DATE(Ora) = ((SELECT MAX(DATE(Ora)) FROM $table));";
    }
    else if($qr == 7)
    {
      $qr = "SELECT Ora,ROUND(AVG(Temperatura),2) AS 'Temperatura' FROM Misurazioni WHERE DATE(Ora) = ((SELECT MAX(DATE(Ora)) FROM $table)) GROUP BY (HOUR(ora));";
    }
    else if($qr == 8)
    {
      $qr = "SELECT TRUNCATE(MIN(Umidita), 2) AS 'Umid_min', TRUNCATE(MAX(Umidita), 2) AS 'Umid_max', TRUNCATE(AVG(Umidita), 2) AS 'Umid_media',DATE(Ora) AS 'Ora', (SELECT TRUNCATE(AVG(Umidita),2) FROM $table WHERE DATE(Ora) = DATE_SUB((SELECT DATE(MAX(Ora)) FROM $table), INTERVAL 1 DAY)) AS 'Umid_media_precedente', DATE_SUB(DATE(Ora),INTERVAL 1 DAY) AS 'Ora_precedente' FROM $table WHERE DATE(Ora) = ((SELECT MAX(DATE(Ora)) FROM $table));";
    }
    else if($qr == 9)
    {
      $qr = "SELECT Ora,ROUND(AVG(Umidita),2) AS 'Umidita' FROM Misurazioni WHERE DATE(Ora) = ((SELECT MAX(DATE(Ora)) FROM $table)) GROUP BY (HOUR(ora));";
    }
    else if($qr == 10)
    {
      $qr = "SELECT TRUNCATE(MIN(Pressione), 2) AS 'Pres_min', TRUNCATE(MAX(Pressione), 2) AS 'Pres_max', TRUNCATE(AVG(Pressione), 2) AS 'Pres_media',DATE(Ora) AS 'Ora', (SELECT TRUNCATE(AVG(Pressione),2) FROM $table WHERE DATE(Ora) = DATE_SUB((SELECT DATE(MAX(Ora)) FROM $table), INTERVAL 1 DAY)) AS 'Pres_media_precedente', DATE_SUB(DATE(Ora),INTERVAL 1 DAY) AS 'Ora_precedente' FROM $table WHERE DATE(Ora) = ((SELECT MAX(DATE(Ora)) FROM $table));";
    }
    else if($qr == 11)
    {
      $qr = "SELECT Ora,ROUND(AVG(Pressione),2) AS 'Pressione' FROM Misurazioni WHERE DATE(Ora) = ((SELECT MAX(DATE(Ora)) FROM $table)) GROUP BY (HOUR(ora));";
    }
    else if($qr == 12)
    {
      $qr = "SELECT TRUNCATE(MIN(CO2), 2) AS 'CO2_min', TRUNCATE(MAX(CO2), 2) AS 'CO2_max', TRUNCATE(AVG(CO2), 2) AS 'CO2_media',DATE(Ora) AS 'Ora', (SELECT TRUNCATE(AVG(CO2),2) FROM $table WHERE DATE(Ora) = DATE_SUB((SELECT DATE(MAX(Ora)) FROM $table), INTERVAL 1 DAY)) AS 'CO2_media_precedente', DATE_SUB(DATE(Ora),INTERVAL 1 DAY) AS 'Ora_precedente' FROM $table WHERE DATE(Ora) = ((SELECT MAX(DATE(Ora)) FROM $table));";
    }
    else if($qr == 13)
    {
      $qr = "SELECT Ora,ROUND(AVG(CO2),2) AS 'CO2' FROM Misurazioni WHERE DATE(Ora) = ((SELECT MAX(DATE(Ora)) FROM $table)) GROUP BY (HOUR(ora));";
    }
    else if($qr == 14)
    {
      $qr = "SELECT MAX(YEAR(Ora)) AS 'MaxYear', MIN(YEAR(Ora)) AS 'MinYear'FROM $table;";
    }
    else if($qr == 101 && isset($_GET['date']))
    {
      $qr = "SELECT HOUR(Ora) AS 'Ora', ROUND(AVG(Temperatura),2) AS 'Temperatura',ROUND(AVG(Umidita),2) AS 'Umidita',ROUND(AVG(Pressione),2) AS 'Pressione',ROUND(AVG(CO2),2) AS 'CO2' FROM Misurazioni WHERE DATE(Ora) = '$date' GROUP BY (HOUR(ora));";
    }
    else
    {
      $qr = "";
    }

    require($_SERVER['DOCUMENT_ROOT'] ."/php/settings.php");
    $result = [];
    if($qr!="")
    {
      $stmt = $connection->query($qr);
      while($row = $stmt->fetch(PDO::FETCH_ASSOC))
      {
        $result[] = $row;
      }
      echo json_encode($result);
    }
  }
}
?>