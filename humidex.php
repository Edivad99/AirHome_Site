<!DOCTYPE html>
<html lang="it">
    <?php
        session_start();
        if(!isset($_SESSION["login"]) || $_SESSION["login"] != "OK")
        {
            header("location: /index.php");
        }     
    ?>
    <head>
        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection" />
        <link type="text/css" rel="stylesheet" href="css/style.css" />
        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>AirHome Dashboard</title>         
        <link rel="shortcut icon" type="image/png" href="/img/favicon.png"/>
    </head>
    <body>
        <div id="nav"></div>
        <h3>Come funziona l'indice Humidex</h3>
        <br>
        <p>L'indice di calore Humidex mostra la temperatura apparente percepita dal corpo umano in situazioni ambientali caratterizzate da alta temperatura dell'aria combinata ad un elevato grado di umidità relativa.</p>
        <br>
        <center>
        <img src="/img/TabellaHumidex.png" style="width:85%;"/>
        </center>
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="js/materialize.min.js"></script>
        <script>
            $(function () {
                $("#nav").load("nav.html");
            });
        </script>
    </body>
</html>