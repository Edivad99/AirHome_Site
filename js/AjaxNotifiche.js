function inviaNotifiche()
{
    ajaxRequest = getXMLHttpRequest();
    if (!ajaxRequest) 
        alert("Request error!");
    var query = document.getElementById("testoNotifica").value;
    if(query=="")
    {
        alert("Inserisci un messaggio da inviare!");
    }
    else
    {
        myURL = "/php/push_notification/test_notifiche.php?mex=" + query;
        ajaxRequest.onreadystatechange = ajaxResponse;
        ajaxRequest.open("GET", myURL);
        ajaxRequest.send(null);
    }
    
} 

function showResult()
{ 
    var names=ajaxRequest.responseXML.getElementsByTagName("notifica");

    for(var i = 0; i<names.length;i++)
    {
        Materialize.toast(names[i].childNodes[0].nodeValue, 4000)
    }
    
}

function getXMLHttpRequest()
{
    var request, err;
    try
    {
        request = new XMLHttpRequest(); // Firefox, Safari, Opera, etc.
    }
    catch(err) 
    {
        try 
        { // first attempt for Internet Explorer
            request = new ActiveXObject("MSXML2.XMLHttp.6.0");
        }
        catch (err) 
        {
            try 
            { // second attempt for Internet Explorer
                request = new ActiveXObject("MSXML2.XMLHttp.3.0");
            }
            catch (err) 
            {
                request = false; // oops, can’t create one!
            }
        }
    }
    return request; 
}

function ajaxResponse() //This gets called when the readyState changes.
{
    if (ajaxRequest.readyState != 4) // check to see if we're done
    { 
        return; 
    }
    else 
    {
        if (ajaxRequest.status == 200) // check to see if successful
        { 
            //Quando tutto è andato bene
            showResult();
        }
        else 
        {
            alert("Request failed: " + ajaxRequest.statusText);
        }
    }
}