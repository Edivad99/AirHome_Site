var font;
var vehicles = [];
function preload()
{
  font = loadFont('js/AirHome/AvenirNextLTPro-Demi.otf');
}
function setup()
{
  var canvas = createCanvas(800,300);
  canvas.parent("logo");
  background(118,191,114);

  var points = font.textToPoints('AirHome',100,200,150);
  var R = random(100,255);
  var G = random(100,255);
  var B = random(100,255);
  R=255;
  G=255;
  B=255;
  for(var i = 0; i < points.length; i++)
  {
    var pt = points[i];
    var vehicle = new Vehicle(pt.x,pt.y,R,G,B);
    vehicles.push(vehicle);
  }
}
function draw()
{
  background(118,191,114);
  for(var i = 0; i < vehicles.length; i++)
  {
    var v = vehicles[i];
    v.behaviors();
    v.update();
    v.show();
  }
}
