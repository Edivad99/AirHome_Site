<!DOCTYPE html>
<html lang="it">
    <?php
        session_start();
        if(!isset($_SESSION["login"]) || $_SESSION["login"] != "OK")
        {
            header("location: /index.php");
        }     
    ?>
    <head>
        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="css/style.css"/>
        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>AirHome Dashboard</title>         
        <link rel="shortcut icon" type="image/png" href="/img/favicon.png"/>
    </head>
    <div id="nav"></div>
    <body>
        <h2 id="titolo">Impostazioni</h2>
        <?php require($_SERVER['DOCUMENT_ROOT'] ."/php/settings.php"); ?>
        <form action="php/savesettings.php" method="post" style="text-align:center;">
            <div>
                <h5 id="notifiche">Scegli le notifiche che vuoi ricevere</h5>
                <div class="switch" style="display:inline-block;width: 200px;">
                    <h5>Notifica Temperatura</h5>
                    <label>
                        Off
                        <input
                        <?php $result = $connection->query("SELECT Valore FROM Impostazioni WHERE Impostazione = 'Temp_Notify';")->fetch(); 
                            if($result["Valore"]=="SI")
                                echo "checked";
                        ?>
                        type="checkbox" name="Temp">
                        <span class="lever"></span>
                        On
                    </label>
                </div>
                <div class="switch" style="display:inline-block;width: 200px;">
                    <h5>Notifica Umidit&agrave;</h5>
                    <label>
                        Off
                        <input
                        <?php $result = $connection->query("SELECT Valore FROM Impostazioni WHERE Impostazione = 'Umid_Notify';")->fetch(); 
                            if($result["Valore"]=="SI")
                                echo "checked";
                        ?>
                        type="checkbox" name="Umid">
                        <span class="lever"></span>
                        On
                    </label>
                </div>
                <div style="clear: left;"></div>
                <div class="switch" style="display:inline-block;width: 200px;">
                    <h5>Notifica Pressione</h5>
                    <label>
                        Off
                        <input
                        <?php $result = $connection->query("SELECT Valore FROM Impostazioni WHERE Impostazione = 'Pres_Notify';")->fetch(); 
                            if($result["Valore"]=="SI")
                                echo "checked";
                        ?>
                        type="checkbox" name="Press">
                        <span class="lever"></span>
                        On
                    </label>
                </div>
                <div class="switch" style="display:inline-block;width: 200px;">
                    <h5>Notifica CO2</h5>
                    <label>
                        Off
                        <input
                        <?php $result = $connection->query("SELECT Valore FROM Impostazioni WHERE Impostazione = 'CO2_Notify';")->fetch(); 
                            if($result["Valore"]=="SI")
                                echo "checked";
                        ?>
                        type="checkbox" name="CO2">
                        <span class="lever"></span>
                        On
                    </label>
                </div>
            </div>
            <div style="clear: left;"></div>
            <input class="waves-effect waves-light btn" style="margin-top:20px;"type ="submit" value="Conferma" name="Notifiche"></input>
        </form>
        <hr>
        <form action="php/savesettings.php" method="post" style="text-align:center;">
            <div>
                <h5 id="range">Imposta il range di tolleranza per le misurazioni</h5>
                <div class="switch" style="display:inline-block;width: 200px;">
                    <h5>Temperatura max</h5>
                    <label>
                        Manuale
                        <input
                        <?php $result = $connection->query("SELECT Valore, Valore_AUTO FROM personalRange WHERE Impostazione = 'Temperatura_max';")->fetch(); 
                            if($result["Valore"]=="Auto")
                                echo "checked";
                        ?>
                        type="checkbox" name="Tmax">
                        <span class="lever"></span>
                        Auto
                        <br>
                        <input
                        <?php 
                            if($result["Valore"]=="Auto")
                                echo "value=\"" .  $result["Valore_AUTO"] . " °C\"";
                            else 
                                echo "value=\"" .  $result["Valore"] . " °C\"";
                        ?> 
                        name="TmaxL" type="text" style="width: 75px;" autocomplete="off">
                    </label>
                </div>
                <div class="switch" style="display:inline-block;width: 200px;">
                    <h5>Umidit&agrave max</h5>
                    <label>
                        Manuale
                        <input
                        <?php $result = $connection->query("SELECT Valore, Valore_AUTO FROM personalRange WHERE Impostazione = 'Umidita_max';")->fetch(); 
                            if($result["Valore"]=="Auto")
                                echo "checked";
                        ?>
                        type="checkbox" name="Umax">
                        <span class="lever"></span>
                        Auto
                        <br>
                        <input
                        <?php 
                            if($result["Valore"]=="Auto")
                                echo "value=\"" .  $result["Valore_AUTO"] . " %\"";
                            else 
                                echo "value=\"" .  $result["Valore"] . " %\"";
                        ?> 
                        name="UmaxL" type="text" style="width: 75px;" autocomplete="off">
                    </label>
                </div>
                <div class="switch" style="display:inline-block;width: 200px;">
                    <h5>Pressione max</h5>
                    <label>
                        Manuale
                        <input
                        <?php $result = $connection->query("SELECT Valore, Valore_AUTO FROM personalRange WHERE Impostazione = 'Press_max';")->fetch(); 
                            if($result["Valore"]=="Auto")
                                echo "checked";
                        ?>
                        type="checkbox" name="Pmax">
                        <span class="lever"></span>
                        Auto
                        <br>
                        <input
                        <?php 
                            if($result["Valore"]=="Auto")
                                echo "value=\"" .  $result["Valore_AUTO"] . " Pa\"";
                            else 
                                echo "value=\"" .  $result["Valore"] . " Pa\"";
                        ?> 
                        name="PmaxL" type="text" style="width: 75px;" autocomplete="off">
                    </label>
                </div>
                <div style="clear: left;"></div>
                <div class="switch" style="display:inline-block;width: 200px;">
                    <h5>Temperatura min</h5>
                    <label>
                        Manuale
                        <input
                        <?php $result = $connection->query("SELECT Valore, Valore_AUTO FROM personalRange WHERE Impostazione = 'Temperatura_min';")->fetch(); 
                            if($result["Valore"]=="Auto")
                                echo "checked";
                        ?>
                        type="checkbox" name="Tmin">
                        <span class="lever"></span>
                        Auto
                        <br>
                        <input
                        <?php 
                            if($result["Valore"]=="Auto")
                                echo "value=\"" .  $result["Valore_AUTO"] . " °C\"";
                            else 
                                echo "value=\"" .  $result["Valore"] . " °C\"";
                        ?> 
                        name="TminL" type="text" style="width: 75px;" autocomplete="off">
                    </label>
                </div>
                <div class="switch" style="display:inline-block;width: 200px;">
                    <h5>Umidit&agrave min</h5>
                    <label>
                        Manuale
                        <input
                        <?php $result = $connection->query("SELECT Valore, Valore_AUTO FROM personalRange WHERE Impostazione = 'Umidita_min';")->fetch(); 
                            if($result["Valore"]=="Auto")
                                echo "checked";
                        ?>
                        type="checkbox" name="Umin">
                        <span class="lever"></span>
                        Auto
                        <br>
                        <input
                        <?php 
                            if($result["Valore"]=="Auto")
                                echo "value=\"" .  $result["Valore_AUTO"] . " %\"";
                            else 
                                echo "value=\"" .  $result["Valore"] . " %\"";
                        ?> 
                        name="UminL" type="text" style="width: 75px;" autocomplete="off">
                    </label>
                </div>
                <div class="switch" style="display:inline-block;width: 200px;">
                    <h5>Pressione min</h5>
                    <label>
                        Manuale
                        <input
                        <?php $result = $connection->query("SELECT Valore, Valore_AUTO FROM personalRange WHERE Impostazione = 'Press_min';")->fetch(); 
                            if($result["Valore"]=="Auto")
                                echo "checked";
                        ?>
                        type="checkbox" name="Pmin">
                        <span class="lever"></span>
                        Auto
                        <br>
                        <input
                        <?php 
                            if($result["Valore"]=="Auto")
                                echo "value=\"" .  $result["Valore_AUTO"] . " Pa\"";
                            else 
                                echo "value=\"" .  $result["Valore"] . " Pa\"";
                        ?> 
                        name="PminL" type="text" style="width: 75px;" autocomplete="off">
                    </label>
                </div>
                <div style="clear: left;"></div>
                <input class="waves-effect waves-light btn" type ="submit" value="Conferma" name="range"></input>
            </div>
        </form>  
        <hr>
        <center>
            <form class="col s12" method="post" style="text-align:center;width:300px;" action="php/changepwd.php" onsubmit="if (this.pwd.value=='') {alert('Aggiungi la password per procedere!'); return false;}">
                <h5 id="cambiapwd">Cambia password</h5>    
                <div class="input-field col s12">
                    <input id="oldpassword" type="password" style="color:white;font-size:20px;" name="oldpwd">
                    <label for="oldpassword" style="color:white;font-size:20px;">Vecchia Password</label>
                </div>
                <div class="input-field col s12">
                    <br>
                    <input id="newpassword" type="password" style="color:white;font-size:20px;" name="newpwd">
                    <label for="newpassword" style="color:white;font-size:20px;">Nuova Password</label>
                    <input class="waves-effect waves-light btn" type="submit" value = "Cambia password">
                </div>
            </form>
        </center>
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="js/materialize.min.js"></script>
        <script>
        $(function(){
            $("#nav").load("nav.html");
        });
        </script>
    </body>
</html>